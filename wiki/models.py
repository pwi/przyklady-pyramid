from persistent.mapping import PersistentMapping
from persistent import Persistent


class Wiki(PersistentMapping):
    __name__ = None
    __parent__ = None


class Page(Persistent):
    def __init__(self, data):
        self.data = data


def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        app_root = Wiki()
        frontpage = Page('To jest strona startowa')
        app_root['Start'] = frontpage
        frontpage.__name__ = 'Start'
        frontpage.__parent__ = app_root
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
