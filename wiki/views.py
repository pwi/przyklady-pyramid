# -*- coding: utf-8 -*-

from docutils.core import publish_parts
import re

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from .models import Page

# Wyrażenie regularne do wyszukiwania WikiSłów
wikiwords = re.compile(ur"\b([A-ZĆŁŚŹŻ]\w+[A-ZĆŁŚŹŻ]+\w+)", re.UNICODE)


# Nie ma renderera, bo nigdy nie będzie pokazywane
@view_config(context='.models.Wiki')
def view_wiki(context, request):
    # Zwrócenie 302, Found i url do przekierowania
    return HTTPFound(location=request.resource_url(context, 'Start'))


@view_config(context='.models.Page', renderer='templates/view.pt')
def view_page(context, request):
    wiki = context.__parent__

    def check(match):
        word = match.group(1)
        if word in wiki:  # Jest strona dla danego WikiSłowa
            page = wiki[word]
            view_url = request.resource_url(page)
            return u'<a href="%s">%s</a>' % (view_url, word)
        else:  # Dana strona jeszcze nie istnieje
            add_url = request.resource_url(wiki, 'add_page', word)
            return u'<a href="%s">%s</a>' % (add_url, word)

    # Przekształcenie tekstu z formatu ReST na HTML
    content = publish_parts(context.data, writer_name='html')['html_body']
    # Podmiana WikiSłów na odnośniki do stron
    content = wikiwords.sub(check, content)
    edit_url = request.resource_url(context, 'edit_page')
    return dict(page=context, content=content, edit_url=edit_url)


@view_config(name='edit_page', context='.models.Page', renderer='templates/edit.pt')
def edit_page(context, request):
    if request.method == 'POST':
        context.data = request.params['body']
        return HTTPFound(location=request.resource_url(context))
    return dict(page=context, save_url=request.resource_url(context, 'edit_page'))


@view_config(name='add_page', context='.models.Wiki', renderer='templates/edit.pt')
def add_page(context, request):
    # Pierwszy element ścieżki po bieżącym kontekście i widoku
    pagename = request.subpath[0]
    if request.method == 'POST':
        body = request.params['body']
        page = Page(body)
        page.__name__ = pagename
        page.__parent__ = context
        # Dodanie nowej strony do kolekcji stron w klasie Wiki
        context[pagename] = page
        return HTTPFound(location=request.resource_url(page))
    save_url = request.resource_url(context, 'add_page', pagename)
    page = Page('')
    page.__name__ = pagename
    page.__parent__ = context
    return dict(page=page, save_url=save_url)