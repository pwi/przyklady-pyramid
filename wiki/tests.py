# -*- coding: utf-8 -*-

import unittest

from pyramid import testing


####################
#   Testy modeli   #
####################


class WikiModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Wiki
        return Wiki

    def _makeOne(self):
        return self._getTargetClass()()

    def test_it(self):
        wiki = self._makeOne()
        self.assertEqual(wiki.__parent__, None)
        self.assertEqual(wiki.__name__, None)


class PageModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Page
        return Page

    def _makeOne(self, data=u'some data'):
        return self._getTargetClass()(data=data)

    def test_constructor(self):
        instance = self._makeOne()
        self.assertEqual(instance.data, u'some data')


class AppmakerTests(unittest.TestCase):

    def _callFUT(self, zodb_root):
        from .models import appmaker
        return appmaker(zodb_root)

    def test_it(self):
        root = {}
        self._callFUT(root)
        self.assertEqual(root['app_root']['Start'].data,
                         'To jest strona startowa')


#####################
#   Testy widoków   #
#####################


class ViewWikiTests(unittest.TestCase):
    def test_it(self):
        from .views import view_wiki
        context = testing.DummyResource()
        request = testing.DummyRequest()
        response = view_wiki(context, request)
        self.assertEqual(response.location, 'http://example.com/Start')


class ViewPageTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import view_page
        return view_page(context, request)

    def test_it(self):
        wiki = testing.DummyResource()
        wiki[u'NaprawdęIstnieje'] = testing.DummyResource()
        context = testing.DummyResource(data=u'Witaj, OkropnyŚwiat NaprawdęIstnieje')
        context.__parent__ = wiki
        context.__name__ = 'strona'
        request = testing.DummyRequest()
        info = self._callFUT(context, request)
        self.assertEqual(info['page'], context)
        self.assertEqual(
            info['content'],
            u'<div class="document">\n'
            u'<p>Witaj, <a href="http://example.com/add_page/Okropny%C5%9Awiat">'
            u'OkropnyŚwiat</a> '
            u'<a href="http://example.com/Naprawd%C4%99Istnieje/">'
            u'NaprawdęIstnieje</a>'
            u'</p>\n</div>\n')
        self.assertEqual(info['edit_url'],
                         'http://example.com/strona/edit_page')


class AddPageTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import add_page
        return add_page(context, request)

    def test_it_notsubmitted(self):
        context = testing.DummyResource()
        request = testing.DummyRequest()
        request.subpath = ['InnaStrona']
        info = self._callFUT(context, request)
        self.assertEqual(info['page'].data,'')
        self.assertEqual(
            info['save_url'],
            request.resource_url(context, 'add_page', 'InnaStrona'))

    def test_it_submitted(self):
        context = testing.DummyResource()
        data = {'form.submitted':True, 'body':'Siemanko!'}
        request = testing.DummyRequest(data, post=data)
        request.subpath = ['AnotherPage']
        self._callFUT(context, request)
        page = context['AnotherPage']
        self.assertEqual(page.data, 'Siemanko!')
        self.assertEqual(page.__name__, 'AnotherPage')
        self.assertEqual(page.__parent__, context)


class EditPageTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import edit_page
        return edit_page(context, request)

    def test_it_notsubmitted(self):
        context = testing.DummyResource()
        request = testing.DummyRequest()
        info = self._callFUT(context, request)
        self.assertEqual(info['page'], context)
        self.assertEqual(info['save_url'],
                         request.resource_url(context, 'edit_page'))

    def test_it_submitted(self):
        context = testing.DummyResource()
        data = {'form.submitted':True, 'body':'Siemanko!'}
        request = testing.DummyRequest(data, post=data)
        response = self._callFUT(context, request)
        self.assertEqual(response.location, 'http://example.com/')
        self.assertEqual(context.data, 'Siemanko!')